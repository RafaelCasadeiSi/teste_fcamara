var app = angular.module('app',['ngRoute']);

app.config(function($routeProvider, $locationProvider)
{
   $locationProvider.html5Mode(true);

   $routeProvider
   .when('/login', {
      templateUrl : 'app/views/login.html',
      controller     : 'LoginController',
      controllerAs: 'vm'
   })
   .when('/produto', {
      templateUrl : 'app/views/produto.html',
      controller  : 'ProdutoController',
      controllerAs: 'vm'
   })   
   .otherwise ({ redirectTo: '/login' });
});