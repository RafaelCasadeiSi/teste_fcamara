(function () {
    'use strict';
 
    angular
        .module('app')
        .factory('ApiService', ApiService);
 
    ApiService.$inject = [];
    function ApiService() {
        var service = {};
 
        service.rotaApi = 'http://localhost:57249';
 
        return service;
    }  
})();