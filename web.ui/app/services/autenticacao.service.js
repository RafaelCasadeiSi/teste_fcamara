(function () {
    'use strict';
 
    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);
 
    AuthenticationService.$inject = ['$http', '$rootScope', '$location', 'ApiService'];
    function AuthenticationService($http, $rootScope, $location, ApiService) {
        var service = {};
 
        service.Login = Login;
 
        return service;
 
        function Login(username, password, callback) {             
            $http({
                    method: 'POST',
                    dataType: 'jsonp',
                    url: ApiService.rotaApi + '/token',
                    data: "username=teste@fcamara.com.br&password=teste&grant_type=password",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {
                    $http.defaults.headers.common['Authorization'] = 'bearer ' + data.access_token;
                    $location.path('/produto');
                }).error(function (error) {
                    
                })
        }
    }  
})();