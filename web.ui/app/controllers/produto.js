(function () {
	'use strict';

	angular
	.module('app')
	.controller('ProdutoController', ProdutoController);

	ProdutoController.$inject = ['$http', '$location','ApiService'];
	function ProdutoController($http, $location, ApiService) {
		var vm = this;

		function obterProdutos() {            

			$http({
				method: 'GET',
				dataType: 'jsonp',
				url: ApiService.rotaApi + '/api/produto',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',	
					'Authorization': $http.defaults.headers.common['Authorization']
				}
			}).success(function (data) {
				vm.Produtos = data;
			}).error(function (error) {
				if (error.status === 401){
					$http.defaults.headers.common['Authorization'] = null;
					$location.path('/login')
				}
			})
		};
		obterProdutos();
	}

})();