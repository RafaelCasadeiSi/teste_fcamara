	
(function () {
    'use strict';
 
    angular
        .module('app')
        .controller('LoginController', LoginController);
 
    LoginController.$inject = ['$location', 'AuthenticationService'];
    function LoginController($location, AuthenticationService) {
        var vm = this;
 	
        function login() {            
            AuthenticationService.Login(vm.username, vm.password);
        };

        vm.login = login;
    }
 
})();