﻿using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace teste_fcamara.Providers
{
    public class AplicacaoOAuthProvider: OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext contexto)
        {
            contexto.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext contexto)
        {            
            if (contexto.UserName == "teste@fcamara.com.br" && contexto.Password == "teste")
            {                
                Claim[] claims = new Claim[] { new Claim(ClaimTypes.Name, contexto.UserName) };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                contexto.Validated(claimsIdentity);
            }

            return Task.FromResult<object>(null);
        }
    }
}