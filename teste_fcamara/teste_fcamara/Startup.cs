﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(teste_fcamara.Startup))]
namespace teste_fcamara
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigurarOAuth(app);
        }
    }
}