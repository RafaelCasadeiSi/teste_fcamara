﻿using MongoDB.Driver;
using System.Configuration;
using System.Linq;
using teste_fcamara.Entidades;

namespace teste_fcamara.Services
{
    public class ProdutoService
    {        
        public ProdutoService()
        {
            GerarProdutos();
        }

        private IMongoCollection<Produto> ObterColecao()
        {
            string conenection = ConfigurationManager.ConnectionStrings["conexaoMongoDB"].ConnectionString;
            MongoClient cliente = new MongoClient(conenection);
            var database = cliente.GetDatabase("teste_fcamara");

            IMongoCollection<Produto> collection = database.GetCollection<Produto>("produtos");
            return collection;
        }

        public void GerarProdutos()
        {
            IMongoCollection<Produto> colecao = ObterColecao();

            if (!colecao.AsQueryable().Any())
            {
                Produto[] produtos = new Produto[] { new Produto(1, "Maçã"), new Produto(2, "Banana"), new Produto(3, "Goiaba") };
                colecao.InsertMany(produtos);
            }       
        }

        public Produto[] ObterProdutos()
        {
            IMongoCollection<Produto> colecao = ObterColecao();

            Produto[] produtos = colecao?.AsQueryable()?.Select(x => new Produto
            {
                Id = x.Id,
                Nome = x.Nome
            }).ToArray();

            return produtos;

        }
    }
}