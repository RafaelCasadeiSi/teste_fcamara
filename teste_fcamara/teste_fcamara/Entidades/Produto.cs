﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace teste_fcamara.Entidades
{    
    public class Produto
    {
        #region [ Construtores ] 

        public Produto() { }

        public Produto(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        #endregion

        #region [ Propriedades ] 

        public int Id { get; set; }
        public string Nome { get; set; }

        #endregion
    }
}