﻿using Microsoft.Owin.Security;
using System.Web;
using System.Web.Http;
using teste_fcamara.Entidades;
using teste_fcamara.Services;

namespace teste_fcamara.Controllers
{
    public class ProdutoController : ApiController
    {
        [Authorize]
        public Produto[] Get()
        {
            ProdutoService produtoService = new ProdutoService();

            Produto[] produtos = produtoService.ObterProdutos();
            return produtos;
        }
    }
}
